# UAS-Prak_PBO



# NO. 1

## Usecase User:
    1. Login dan mengakses digital banking web service perusahaan.
    2. Melihat saldo rekening perusahaan dan informasi transaksi terkini.
    3. Melakukan transfer antar rekening perusahaan.
    4. Mengelola tagihan dan pembayaran perusahaan.
    5. Melihat laporan keuangan perusahaan.
    6. Mengatur notifikasi dan pemberitahuan terkait transaksi.

## Usecase Manajemen Perusahaan:
    1. Mengakses dashboard manajemen perusahaan melalui digital banking web service.
    2. Melihat dan mengelola saldo rekening perusahaan secara keseluruhan.
    3. Melakukan analisis keuangan perusahaan berdasarkan laporan dan data yang disediakan.
    4. Mengelola arus kas perusahaan.
    5. Mengatur dan melacak anggaran perusahaan.
    6. Mengelola dan memantau transaksi yang dilakukan oleh karyawan.
    7. Mengakses fitur penggajian dan manajemen keuangan karyawan.
    8. Mengelola persetujuan dan otorisasi transaksi yang dilakukan oleh karyawan.
    9. Melakukan pengelolaan risiko keuangan perusahaan.

## Usecase Direksi Perusahaan:
    1. Mengakses dashboard eksekutif melalui digital banking web service.
    2. Melihat ringkasan keuangan perusahaan secara keseluruhan.
    3. Memonitor kinerja dan pertumbuhan perusahaan.
    4. Melihat laporan keuangan dan analisis mendalam.
    5. Mengakses informasi strategis terkait keputusan bisnis.
    6. Mengelola proyeksi keuangan dan perencanaan jangka panjang perusahaan.
    7. Mengakses informasi pasar dan tren terkini.
    8. Mengatur dan memantau tingkat kepatuhan perusahaan terhadap peraturan dan kebijakan keuangan.
    9. Melakukan evaluasi risiko dan pengambilan keputusan berdasarkan analisis data.


# NO. 2
```mermaid
classDiagram
    class Redux Store {
        <<Provider>>
    }

    class App {
        <<Container>>
    }

    class Layout {
        <<Container>>
    }

    class BaseRouter {
        <<Router>>
    }

    class Login {
        <<Container>>
    }
    class Register {
        <<Container>>
    }

    class Dashboard {
        <<Container>>
    }

    class Transactions {
        <<Container>>
    }

    class Transfer {
        <<Container>>
    }

    class Pembayaran {
        <<Container>>
    }

    class Component {

    }

    Redux Store --> Login
    Redux Store --> Register
    Redux Store --> Transfer
    Redux Store --> Pembayaran
    Redux Store --> Transactions
    Redux Store --> Dashboard
    Redux Store --> App
    App --> Layout
    Layout --> BaseRouter
    BaseRouter --> Login
    BaseRouter --> Register
    BaseRouter --> Transfer
    BaseRouter --> Transactions
    BaseRouter --> Dashboard
    BaseRouter --> Pembayaran

    Login --|> Component
    Register --|> Component
    Transfer --|> Component
    Transactions --|> Component
    Dashboard --|> Component
    Pembayaran --|> Component
```
# NO. 3

![jawaban-3](https://gitlab.com/Fr0faive/uas-prak_pbo/-/raw/main/GIF/no-3-s.gif)
![jawaban-3-1](https://gitlab.com/Fr0faive/uas-prak_pbo/-/raw/main/GIF/no-3-all.gif)

# NO. 4
> "Presentational-Container Component", di mana Pola ini memisahkan komponen ke dalam dua kategori yang berbeda: komponen kontainer (containers) dan komponen presentasional (components).

![jawaban-4](https://gitlab.com/Fr0faive/uas-prak_pbo/-/raw/main/GIF/no-4.gif)

# NO. 5

![jawaban-5](https://gitlab.com/Fr0faive/uas-prak_pbo/-/raw/main/GIF/no-5.gif)

# NO. 6

![jawaban-6](https://gitlab.com/Fr0faive/uas-prak_pbo/-/raw/main/GIF/no-6.gif)

# NO. 7

![jawaban-7](https://gitlab.com/Fr0faive/uas-prak_pbo/-/raw/main/GIF/no-7.gif)

# NO. 8

![jawaban-8](https://gitlab.com/Fr0faive/uas-prak_pbo/-/raw/main/GIF/no-8.gif)

# NO. 9

[Youtube](https://youtu.be/e9IHrGSEqCw)
