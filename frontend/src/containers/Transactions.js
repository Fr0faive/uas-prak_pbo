import React, { Component } from "react";
import { PageHeader, Table } from "react-bootstrap";
import axios from 'axios';

class Transactions extends Component {
    constructor(props) {
        super(props);

        this.state = {
          transactions: [],
        };
    }

    componentDidMount() {

        axios.get("http://localhost:4000/api/transactions",{
            user: this.props.location.state.user,
        })
            .then(response => {
                this.setState({ transactions: response.data });
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        return (
            <div className="trans">
                <PageHeader>Your Transactions</PageHeader>
                <Table>
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Transaction</th>
                            <th>Deposit</th>
                            <th>Credit</th>
                            <th>Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.transactions.map(tran => (
                            <tr key={tran.id}>
                                <td>{tran.date}</td>
                                <td>{tran.transaction}</td>
                                <td>{tran.deposit}</td>
                                <td>{tran.credit}</td>
                                <td>{tran.balance}</td>
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>
        );
    }
}

export default Transactions;
