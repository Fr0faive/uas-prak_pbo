import React from "react";
import "../css/NotFound.css";

export default () =>
  <div className="NotFound">
    <h2>Page not found!</h2>
  </div>;
