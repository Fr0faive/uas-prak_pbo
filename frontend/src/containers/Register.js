import React, { Component } from 'react';
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "../css/Register.css";
import axios from 'axios';

class Register extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: '',
            email: '',
            password: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    validateForm() {
        return this.state.user.length > 0 && this.state.password.length > 0 && this.state.email.length > 0;
    }

    handleChange(event) {
        this.setState({[event.target.id] : event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        axios.post("http://localhost:4000/api/register", {
            user: this.state.user,
            email: this.state.email,
            password: this.state.password,
        })
            .then(response => {
                if (response.data.length > 0) {
                    alert("Sukses Mendaftar!")
                } else {
                    alert("Gagal mendaftar!");
                    this.setState({ user: '' });
                    this.setState({ email: '' });
                    this.setState({ password: '' });
                }
            })
            .catch(err => console.log(err));

    }

    render() {
        return (
            <div className="Register">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="user" bsSize="large">
                        <ControlLabel>UserName</ControlLabel>
                        <FormControl
                            autoFocus
                            type="user"
                            value={this.state.user}
                            onChange={this.handleChange}
                         />
                    </FormGroup>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>E-Mail</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                         />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            type="password"
                            value={this.state.password}
                            onChange={this.handleChange}
                    />
                    </FormGroup>
                    <Button id="submit" block bsSize="large" disabled={!this.validateForm()} type="submit">
                        SignUp
                    </Button>
                </form>
            </div>
        );
    }
}
export default Register;
