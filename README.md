# F-Bank
Frontend: ReactJS
Backend: NodeJS/Express + MySQL

1. Run frontend at localhost:3000

     $ cd frontend
  
     $ npm start

2. Run backend at localhost:4000

     $ cd backend/src
  
     $ node server.js

