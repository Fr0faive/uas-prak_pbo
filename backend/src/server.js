const express = require('express');
const cors = require('cors');
const mysql = require('mysql');
const bcrypt = require('bcrypt');

const con = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'webapp_bni'
});

const app = express();

con.connect(function (err) {
    if (err) throw err;
    console.log("Connected to database.");
});

app.use(cors());

app.use(express.json()); // Menambahkan middleware untuk parsing body berformat JSON

app.post('/api/login', function (req, res) {
    const user = req.body.user;
    const password = req.body.password;

    con.query('SELECT * FROM users WHERE username = ?', [user], function (err, results) {
        if (err) {
            res.status(500).send(err);
        } else {
            if (results.length > 0) {
                const hashedPassword = results[0].password; // Assuming password field name is "password"

                bcrypt.compare(password, hashedPassword, function (err, match) {
                    if (err) {
                        res.status(500).send(err);
                    } else if (match) {
                        res.send(results);
                    } else {
                        res.send([]);
                    }
                });
            } else {
                res.send([]);
            }
        }
    });
});

app.post('/api/register', function(req, res) {
    const user = req.body.user;
    const email = req.body.email;
    const password = req.body.password;
  
    // Hash the password
    bcrypt.hash(password, 10, function(err, hashedPassword) {
      if (err) {
        res.status(500).send(err);
      } else {
        // Insert user into the database
        con.query(
          `INSERT INTO users (username, email , password) VALUES (?, ?, ?)`,
          [user, email , hashedPassword],
          function(err, results) {
            if (err) {
              res.status(500).send(err);
            } else {
              res.send('User registered successfully');
            }
          }
        );
      }
    });
  });

app.get('/api/transactions', function (req, res) {
    const user = req.query.user;

    con.query(`SELECT * FROM transactions WHERE username = ? ORDER BY date DESC`, [user], function (err, results) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.send(results);
        }
    });
});

app.get('/api/data', function (req, res) {
    con.query(`SELECT * FROM users`, function (err, result) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.send(result);
        }
    });
});

app.listen(4000, () => {
    console.log('Running on port 4000');
});
